{ config, pkgs, ... }:
{
  services = {

    xserver = {
      enable = true;
      xkb.layout = "it";
      # displayManager.gdm.enable = true;
      # desktopManager.gnome.enable = true;
      desktopManager.xterm.enable = false;
      excludePackages = [ pkgs.xterm ];
    };

    displayManager = {
      sddm.enable = true;
      sddm.wayland.enable = true;
    };

    desktopManager.plasma6.enable = true;

    libinput.enable = true;

    # pcscd.enable = true;

    printing.enable = true;

    mozillavpn.enable = true;

    pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
    };

    btrfs.autoScrub = {
      enable = true;
      interval = "weekly";
      fileSystems = [ "/" ];
    };

    tailscale = {
      enable = true;
    };

    resolved.enable = true;

    avahi = {
      enable = true;
      nssmdns4 = true;
      openFirewall = true;
      domainName = "internal";
      publish.enable = true;
    };

  };
}
