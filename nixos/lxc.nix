{ ... }:

{

  imports = [
    ./lxd/lxd-preseed.nix
    ./lxd/lxd-networking.nix
  ];

  virtualisation = {
    # Enable libVirtd
    libvirtd.enable = true;

    # Enable LXC containers
    lxd = {
      enable = true;

      # This turns on a few sysctl settings that the LXD documentation recommends
      # for running in production.
      recommendedSysctlSettings = true;
    };

    # This enables lxcfs, which is a FUSE fs that sets up some things so that
    # things like /proc and cgroups work better in lxd containers.
    # See https://linuxcontainers.org/lxcfs/introduction/ for more info.
    #
    # Also note that the lxcfs NixOS option says that in order to make use of
    # lxcfs in the container, you need to include the following NixOS setting
    # in the NixOS container guest configuration:
    #
    # virtualisation.lxc.defaultConfig = "lxc.include = ''${pkgs.lxcfs}/share/lxc/config/common.conf.d/00-lxcfs.conf";
    lxc.lxcfs.enable = true;

  };

  # networking.firewall.trustedInterfaces = [ "lxdbr0" ];

  # kernel module for forwarding to work
  boot.kernelModules = [ "nf_nat_ftp" ];

}
