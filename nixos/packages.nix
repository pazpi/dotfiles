# List packages installed in system profile. To search, run:
# $ nix search wget

{ pkgs, ... }:
{
  # Allow unfree packages
  nixpkgs.config = {
    allowUnfree = true;
    nvidia.acceptLicense = true;
    permittedInsecurePackages = [
      "electron-25.9.0"
    ];
  };

  environment = {
    shells = with pkgs; [ zsh ];
    systemPackages = with pkgs; [
      adi1090x-plymouth-themes
      adw-gtk3
      atool
      bat
      bitwarden
      blender
      # dconf
      # dconf2nix
      # evince
      fd
      file
      firefox
      fzf
      git
      gitui
      # gnome-extension-manager
      # gnome.dconf-editor
      # gnome.gnome-tweaks
      gnupg
      inkscape
      jq
      libreoffice-qt
      logitech-udev-rules
      mission-center
      mpv
      nextcloud-client
      nomacs
      obsidian
      # peazip
      pdfarranger
      ranger
      ripgrep
      speedcrunch
      spotify
      starship
      steam
      # tdesktop
      telegram-desktop
      thunderbird
      tree
      unzip
      vim
      vlc
      vscodium
      w3m
      wget
      zsh
    ];

    sessionVariables = {
      # NIXOS_OZONE_WL = "1";
      # QT_AUTO_SCREEN_SCALE_FACTOR = "auto";
    };

    # Remove standard Gnome Packages
    gnome.excludePackages = (with pkgs; [
      gnome-photos
      gnome-tour
      gnome-connections
      gnome-photos
      gedit # text editor
    ]) ++ (with pkgs.gnome; [
      atomix # puzzle game
      cheese # webcam tool
      epiphany # web browser
      evince # document viewer
      geary # email reader
      gnome-calendar
      gnome-characters
      gnome-clocks
      gnome-contacts
      gnome-font-viewer
      gnome-maps
      gnome-music
      gnome-terminal
      gnome-weather
      hitori # sudoku game
      iagno # go game
      tali # poker game
      totem # video player
      yelp # help viewer
    ]);
  };

  programs = {
    # dconf.enable = true;
    zsh.enable = true;
    # hyprland.enable = false;
    virt-manager.enable = true;

    gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
    };

  };

}
