{ pkgs, ... }:
{
  defaultEditor = true;

  plugins = with pkgs.vimPlugins; [
    vim-clap
    switch-vim # switch between two world (TRUE<->FALSE)
    vim-easy-align # allign text
    vim-highlightedyank # see what you have yanked

    # Vim Functional Enchancements
    nerdcommenter # quick comment line of code
    ale # error checking multiple language
    tagbar # left panel with tag (es function or macro for different programming language)

    # GUI enhancements
    vim-airline # status line
    vim-airline-themes # themes for airline

    # Git
    vim-gitgutter # git diff next to line number
    vim-fugitive # manage git inside vim
    gv-vim # git tree :GV

    # Syntactic language support
    vim-polyglot # A collection of language packs for Vim.

    # Fuzzy Finders
    fzf-vim # Fzf fuzzy finder

    # Colorschema
    # catppuccin-vim
  ];

  settings = {
    background = "dark";
    copyindent = true;
    directory = [ "~/.vim/.tmp" "~/tmp" "/tmp" ];
    hidden = true;
    history = 3000;
    ignorecase = true;
    modeline = false;
    mouse = "a";
    number = true;
    relativenumber = true;
    shiftwidth = 4;
    smartcase = true;
    tabstop = 4;
    undofile = true;
    undodir = [ "~/.vim/undo" "~/tmp" "/tmp" ];
  };

  extraConfig =
    ''
      let mapleader=","
      let mapocalleader="\\"
      set termguicolors
      set clipboard=unnamedplus " normal OS clipboard interaction
      set foldenable
      set incsearch
      set pastetoggle=<F2> " when in insert mode, press <F2> to go to paste mode, where you can paste mass data that won't be autoindented
      set scrolloff=4                " keep 4 lines off the edges of the screen when scrolling
      set wrap
      set wildmenu                    " make tab completion for files/buffers act like bash
      set wildmode=longest:full,full			" show a list when pressing tab and complete first full match
      set wildignore=*.swp,*.bak,*.pyc,*.class,.hg,.svn,*~,*.png,*.jpg,*.gif
      set title                       " change the terminal's title
    '';

}
