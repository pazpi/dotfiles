# Generated via dconf2nix: https://github.com/gvolpe/dconf2nix
{ lib, ... }:

with lib.hm.gvariant;

{
  dconf.settings = {
    "apps/seahorse/listing" = {
      keyrings-selected = [ "openssh:///home/pazpi/.ssh" ];
    };
    "com/mattjakeman/ExtensionManager" = {
      last-used-version = "0.4.2";
    };

    "io/missioncenter/MissionCenter" = {
      apps-page-merged-process-stats = true;
      apps-page-sorting-column = "None";
      apps-page-sorting-order = "None";
      performance-page-cpu-graph = 2;
      performance-page-kernel-times = true;
      performance-selected-page = "cpu";
      window-height = 677;
      window-selected-page = "performance-page";
      window-width = 958;
    };

    "org/gnome/Connections" = {
      first-run = false;
    };

    "org/gnome/Console" = {
      last-window-size = mkTuple [ 720 1048 ];
    };

    "org/gnome/Snapshot" = {
      is-maximized = false;
      window-height = 640;
      window-width = 800;
    };

    "org/gnome/TextEditor" = {
      highlight-current-line = true;
      show-line-numbers = true;
    };

    "org/gnome/calculator" = {
      accuracy = 9;
      angle-units = "degrees";
      base = 10;
      button-mode = "advanced";
      number-format = "automatic";
      show-thousands = false;
      show-zeroes = false;
      source-currency = "DZD";
      source-units = "degree";
      target-currency = "DZD";
      target-units = "degree";
      window-maximized = false;
      window-size = mkTuple [ 680 660 ];
      word-size = 64;
    };

    "org/gnome/desktop/a11y/applications" = {
      screen-reader-enabled = false;
    };

    "org/gnome/desktop/app-folders" = {
      folder-children = [ "Utilities" "altro-1" "c9bdfef3-2393-49f8-bee2-e663e6977547" "55214476-dea8-4367-bcc9-31de9100737e" "972ad209-9f2a-4cd4-9d90-f6200ad52878" "48a96077-adb7-480d-a251-85848fd85453" "d564ff33-887e-458a-8c61-8dc24c2d9ef6" "c8aa2fb0-8b4e-4633-a91a-bbbdc6f0fc4f" "b575d558-d031-49c6-97f6-1807b899ef3c" "0da9164c-4714-4b2c-a789-2acae1fca889" ];
    };

    "org/gnome/desktop/app-folders/folders" = {
      apps = [ "org.gnome.Maps.desktop" ];
      name = "Utilities";
      translate = true;
    };

    "org/gnome/desktop/app-folders/folders/0db77aa2-5e38-41a2-b8be-e2be3faed5d1" = {
      apps = [ "org.gnome.Weather.desktop" "org.gnome.Contacts.desktop" "org.gnome.clocks.desktop" "org.gnome.Calculator.desktop" "org.gnome.Settings.desktop" "gnome-system-monitor.desktop" "yelp.desktop" "org.gnome.font-viewer.desktop" "org.gnome.baobab.desktop" "org.gnome.DiskUtility.desktop" "org.gnome.Logs.desktop" ];
      name = "Utilities";
      translate = false;
    };

    "org/gnome/desktop/app-folders/folders/48a96077-adb7-480d-a251-85848fd85453" = {
      apps = [ "ca.desrt.dconf-editor.desktop" "org.gnome.baobab.desktop" "org.gnome.Settings.desktop" "gnome-system-monitor.desktop" "org.gnome.DiskUtility.desktop" "org.gnome.Logs.desktop" ];
      name = "System";
      translate = false;
    };

    "org/gnome/desktop/app-folders/folders/55214476-dea8-4367-bcc9-31de9100737e" = {
      apps = [ "org.gnome.FileRoller.desktop" "org.gnome.Console.desktop" "org.gnome.Calculator.desktop" "org.gnome.Snapshot.desktop" "cups.desktop" "org.gnome.tweaks.desktop" "org.speedcrunch.SpeedCrunch.desktop" ];
      name = "Utilities";
      translate = false;
    };

    "org/gnome/desktop/app-folders/folders/930f22fc-77ad-4fc4-83d6-52d4479240ba" = {
      apps = [ "gnome-system-monitor.desktop" "org.gnome.Settings.desktop" "org.gnome.Logs.desktop" "org.gnome.baobab.desktop" "yelp.desktop" "org.gnome.font-viewer.desktop" "org.gnome.DiskUtility.desktop" "xterm.desktop" ];
      name = "Admins";
      translate = false;
    };

    "org/gnome/desktop/app-folders/folders/972ad209-9f2a-4cd4-9d90-f6200ad52878" = {
      apps = [ "xterm.desktop" "org.gnome.clocks.desktop" "org.gnome.Calendar.desktop" "org.gnome.font-viewer.desktop" "org.gnome.Connections.desktop" ];
      name = "Unnamed Folder";
    };

    "org/gnome/desktop/app-folders/folders/Pardus" = {
      categories = [ "X-Pardus-Apps" ];
      name = "X-Pardus-Apps.directory";
      translate = true;
    };

    "org/gnome/desktop/app-folders/folders/Utilities" = {
      apps = [ "gnome-abrt.desktop" "gnome-system-log.desktop" "nm-connection-editor.desktop" "org.gnome.baobab.desktop" "org.gnome.Connections.desktop" "org.gnome.DejaDup.desktop" "org.gnome.Dictionary.desktop" "org.gnome.DiskUtility.desktop" "org.gnome.Evince.desktop" "org.gnome.FileRoller.desktop" "org.gnome.fonts.desktop" "org.gnome.Loupe.desktop" "org.gnome.seahorse.Application.desktop" "org.gnome.tweaks.desktop" "org.gnome.Usage.desktop" "vinagre.desktop" ];
      categories = [ "X-GNOME-Utilities" ];
      name = "X-GNOME-Utilities.directory";
      translate = true;
    };

    "org/gnome/desktop/app-folders/folders/altro-1" = {
      apps = [ "org.gnome.Maps.desktop" "org.gnome.Contacts.desktop" ];
      name = "Altro";
      translate = false;
    };

    "org/gnome/desktop/app-folders/folders/b575d558-d031-49c6-97f6-1807b899ef3c" = {
      apps = [ "bitwarden.desktop" "com.nextcloud.desktopclient.nextcloud.desktop" "thunderbird.desktop" "mozillavpn.desktop" ];
      name = "Personal";
      translate = false;
    };

    "org/gnome/desktop/app-folders/folders/c8aa2fb0-8b4e-4633-a91a-bbbdc6f0fc4f" = {
      apps = [ "vim.desktop" "code.desktop" "pycharm-community.desktop" "nixos-manual.desktop" "gvim.desktop" ];
      name = "Code";
      translate = false;
    };

    "org/gnome/desktop/app-folders/folders/c9bdfef3-2393-49f8-bee2-e663e6977547" = {
      apps = [ "base.desktop" "startcenter.desktop" "calc.desktop" "math.desktop" "writer.desktop" "impress.desktop" "draw.desktop" "simple-scan.desktop" "org.gnome.Evince.desktop" "com.github.jeromerobert.pdfarranger.desktop" ];
      name = "Office";
    };

    "org/gnome/desktop/app-folders/folders/d564ff33-887e-458a-8c61-8dc24c2d9ef6" = {
      apps = [ "blender.desktop" "org.inkscape.Inkscape.desktop" "org.gnome.Loupe.desktop" "mpv.desktop" "vlc.desktop" "spotify.desktop" "org.nomacs.ImageLounge.desktop" ];
      name = "Graphics/Audio/Video";
      translate = false;
    };

    "org/gnome/desktop/app-folders/folders/e186533f-c15d-491e-bbed-4e1132fe9f50" = {
      apps = [ "math.desktop" "writer.desktop" "impress.desktop" "draw.desktop" "calc.desktop" "base.desktop" "startcenter.desktop" ];
      name = "Office";
    };

    "org/gnome/desktop/calendar" = {
      show-weekdate = true;
    };

    "org/gnome/desktop/input-sources" = {
      sources = [ (mkTuple [ "xkb" "it" ]) ];
      xkb-options = [ "terminate:ctrl_alt_bksp" ];
    };

    "org/gnome/desktop/interface" = {
      # clock-show-weekday = false;
      # color-scheme = "default";
      # # cursor-size = 32;
      # # cursor-theme = "catppuccin-macchiato-dark-cursors";
      # document-font-name = "FiraCode Nerd Font 11";
      # enable-animations = true;
      # font-antialiasing = "rgba";
      # font-hinting = "slight";
      # font-name = "FiraCode Nerd Font 10";
      # gtk-theme = "Catppuccin-Macchiato-Compact-Pink-Dark";
      # # icon-theme = "catppuccin-macchiato-dark-cursors";
      # monospace-font-name = "FiraCode Nerd Font Mono 10";
      # scaling-factor = mkUint32 2;
      # show-battery-percentage = true;
      # text-scaling-factor = 1.0;
      # toolbar-style = "text";
    };

    "org/gnome/desktop/notifications" = {
      application-children = [ "org-gnome-nautilus" "org-gnome-console" "firefox" "org-gnome-texteditor" "org-telegram-desktop" "thunderbird" "gnome-power-panel" "steam" "gnome-network-panel" "calc" "obsidian" "codium" "peazip" "com-nextcloud-desktopclient-nextcloud" ];
      show-banners = true;
    };

    "org/gnome/desktop/notifications/application/calc" = {
      application-id = "calc.desktop";
    };

    "org/gnome/desktop/notifications/application/code" = {
      application-id = "code.desktop";
    };

    "org/gnome/desktop/notifications/application/codium" = {
      application-id = "codium.desktop";
    };

    "org/gnome/desktop/notifications/application/com-mattjakeman-extensionmanager" = {
      application-id = "com.mattjakeman.ExtensionManager.desktop";
    };

    "org/gnome/desktop/notifications/application/com-nextcloud-desktopclient-nextcloud" = {
      application-id = "com.nextcloud.desktopclient.nextcloud.desktop";
    };

    "org/gnome/desktop/notifications/application/firefox" = {
      application-id = "firefox.desktop";
    };

    "org/gnome/desktop/notifications/application/gitkraken" = {
      application-id = "GitKraken.desktop";
    };

    "org/gnome/desktop/notifications/application/gnome-network-panel" = {
      application-id = "gnome-network-panel.desktop";
    };

    "org/gnome/desktop/notifications/application/gnome-power-panel" = {
      application-id = "gnome-power-panel.desktop";
    };

    "org/gnome/desktop/notifications/application/jetbrains-pycharm-42420912-3315-4683-b62e-fe413bf0be8e" = {
      application-id = "jetbrains-pycharm-42420912-3315-4683-b62e-fe413bf0be8e.desktop";
    };

    "org/gnome/desktop/notifications/application/jetbrains-rider-de95da45-d3f5-4b1f-9830-71d3f60c60f8" = {
      application-id = "jetbrains-rider-de95da45-d3f5-4b1f-9830-71d3f60c60f8.desktop";
    };

    "org/gnome/desktop/notifications/application/kitty" = {
      application-id = "kitty.desktop";
    };

    "org/gnome/desktop/notifications/application/mpv" = {
      application-id = "mpv.desktop";
    };

    "org/gnome/desktop/notifications/application/obsidian" = {
      application-id = "obsidian.desktop";
    };

    "org/gnome/desktop/notifications/application/org-gnome-baobab" = {
      application-id = "org.gnome.baobab.desktop";
    };

    "org/gnome/desktop/notifications/application/org-gnome-console" = {
      application-id = "org.gnome.Console.desktop";
    };

    "org/gnome/desktop/notifications/application/org-gnome-extensions-desktop" = {
      application-id = "org.gnome.Extensions.desktop.desktop";
    };

    "org/gnome/desktop/notifications/application/org-gnome-extensions" = {
      application-id = "org.gnome.Extensions.desktop";
    };

    "org/gnome/desktop/notifications/application/org-gnome-fileroller" = {
      application-id = "org.gnome.FileRoller.desktop";
    };

    "org/gnome/desktop/notifications/application/org-gnome-nautilus" = {
      application-id = "org.gnome.Nautilus.desktop";
    };

    "org/gnome/desktop/notifications/application/org-gnome-settings" = {
      application-id = "org.gnome.Settings.desktop";
    };

    "org/gnome/desktop/notifications/application/org-gnome-texteditor" = {
      application-id = "org.gnome.TextEditor.desktop";
    };

    "org/gnome/desktop/notifications/application/org-telegram-desktop" = {
      application-id = "org.telegram.desktop.desktop";
    };

    "org/gnome/desktop/notifications/application/pycharm-community" = {
      application-id = "pycharm-community.desktop";
    };

    "org/gnome/desktop/notifications/application/solaar" = {
      application-id = "solaar.desktop";
    };

    "org/gnome/desktop/notifications/application/spotify" = {
      application-id = "spotify.desktop";
    };

    "org/gnome/desktop/notifications/application/steam" = {
      application-id = "steam.desktop";
    };

    "org/gnome/desktop/notifications/application/thunderbird" = {
      application-id = "thunderbird.desktop";
    };

    "org/gnome/desktop/peripherals/mouse" = {
      left-handed = false;
      speed = -0.296875;
    };

    "org/gnome/desktop/peripherals/touchpad" = {
      speed = 0.0;
      tap-to-click = true;
      two-finger-scrolling-enabled = true;
    };

    "org/gnome/desktop/search-providers" = {
      sort-order = [ "org.gnome.Contacts.desktop" "org.gnome.Documents.desktop" "org.gnome.Nautilus.desktop" ];
    };

    "org/gnome/desktop/sound" = {
      event-sounds = false;
      theme-name = "__custom";
    };

    "org/gnome/desktop/wm/keybindings" = {
      show-desktop = [ "<Super>d" ];
      switch-applications = [ "<Super>Tab" ];
      switch-applications-backward = [ "<Shift><Super>Tab" ];
      switch-windows = [ "<Alt>Tab" ];
      switch-windows-backward = [ "<Shift><Alt>Tab" ];
    };

    "org/gnome/desktop/wm/preferences" = {
      button-layout = "appmenu:minimize,maximize,close";
      num-workspaces = 2;
      titlebar-font = "FiraCode Nerd Font 11";
    };

    "org/gnome/evince/default" = {
      continuous = true;
      dual-page = false;
      dual-page-odd-left = true;
      enable-spellchecking = true;
      fullscreen = false;
      inverted-colors = false;
      show-sidebar = true;
      sidebar-page = "thumbnails";
      sidebar-size = 148;
      sizing-mode = "free";
      window-ratio = mkTuple [ 1.612578 1.244774 ];
      zoom = 0.864261;
    };

    "org/gnome/evolution-data-server" = {
      migrated = true;
    };

    "org/gnome/file-roller/dialogs/extract" = {
      recreate-folders = true;
      skip-newer = false;
    };

    "org/gnome/file-roller/listing" = {
      list-mode = "as-folder";
      name-column-width = 250;
      show-path = false;
      sort-method = "name";
      sort-type = "ascending";
    };

    "org/gnome/file-roller/ui" = {
      sidebar-width = 200;
      window-height = 480;
      window-width = 600;
    };

    "org/gnome/gnome-system-monitor" = {
      current-tab = "resources";
      maximized = false;
      network-in-bits = true;
      network-total-in-bits = true;
      network-total-unit = true;
      show-dependencies = false;
      show-whose-processes = "user";
      window-state = mkTuple [ 700 500 ];
    };

    "org/gnome/gnome-system-monitor/disktreenew" = {
      col-6-visible = true;
      col-6-width = 0;
    };

    "org/gnome/gnome-system-monitor/proctree" = {
      columns-order = [ 0 1 2 3 4 6 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 ];
      sort-col = 8;
      sort-order = 0;
    };

    "org/gnome/meld" = {
      wrap-mode = "none";
    };

    "org/gnome/meld/window-state" = {
      height = 1004;
      is-maximized = true;
      width = 960;
    };

    "org/gnome/mutter" = {
      edge-tiling = true;
    };

    "org/gnome/nautilus/compression" = {
      default-compression-format = "zip";
    };

    "org/gnome/nautilus/list-view" = {
      default-column-order = [ "name" "size" "type" "owner" "group" "permissions" "date_modified" "date_accessed" "date_created" "recency" "detailed_type" ];
      default-visible-columns = [ "name" "size" "type" "date_modified" ];
    };

    "org/gnome/nautilus/preferences" = {
      default-folder-viewer = "list-view";
      migrated-gtk-settings = true;
      search-filter-time-type = "last_modified";
      search-view = "list-view";
    };

    "org/gnome/nautilus/window-state" = {
      initial-size = mkTuple [ 890 550 ];
      maximized = true;
    };

    "org/gnome/nm-applet/eap/5d537878-28ee-4cae-a03a-ee2f48010542" = {
      ignore-ca-cert = false;
      ignore-phase2-ca-cert = false;
    };

    "org/gnome/portal/filechooser/kitty" = {
      last-folder-path = "/home/pazpi/Documents/Flavia USB";
    };

    "org/gnome/settings-daemon/plugins/color" = {
      night-light-enabled = true;
      night-light-temperature = mkUint32 3700;
    };

    "org/gnome/settings-daemon/plugins/media-keys" = {
      home = [ "<Super>e" ];
      search = [ "<Super>f" ];
      www = [ "<Super>w" ];
    };

    "org/gnome/settings-daemon/plugins/power" = {
      sleep-inactive-ac-type = "nothing";
    };

    "org/gnome/shell" = {
      disable-user-extensions = false;
      disabled-extensions = [ "apps-menu@gnome-shell-extensions.gcampax.github.com" "drive-menu@gnome-shell-extensions.gcampax.github.com" ];
      enabled-extensions = [ "blur-my-shell@aunetx" "caffeine@patapon.info" "dash-to-dock@micxgx.gmail.com" "gsconnect@andyholmes.github.io" "gTile@vibou" "just-perfection-desktop@just-perfection" "pano@elhan.io" "quick-settings-tweaks@qwreey" "rounded-window-corners@yilozt" "Vitals@CoreCoding.com" "trayIconsReloaded@selfmade.pl" "openweather-extension@jenslody.de" "tailscale-status@maxgallup.github.com" ];
      favorite-apps = [ "org.gnome.Nautilus.desktop" "firefox.desktop" "kitty.desktop" "org.telegram.desktop.desktop" "steam.desktop" "code.desktop" "obsidian.desktop" ];
      last-selected-power-profile = "power-saver";
      welcome-dialog-last-shown-version = "44.2";
    };

    "org/gnome/shell/extensions/blur-my-shell" = {
      settings-version = 2;
    };

    "org/gnome/shell/extensions/blur-my-shell/appfolder" = {
      brightness = 0.6;
      sigma = 30;
    };

    "org/gnome/shell/extensions/blur-my-shell/dash-to-dock" = {
      blur = true;
      brightness = 0.6;
      sigma = 30;
      static-blur = true;
      style-dash-to-dock = 0;
    };

    "org/gnome/shell/extensions/blur-my-shell/panel" = {
      brightness = 0.6;
      sigma = 30;
    };

    "org/gnome/shell/extensions/blur-my-shell/window-list" = {
      brightness = 0.6;
      sigma = 30;
    };

    "org/gnome/shell/extensions/caffeine" = {
      countdown-timer = 60;
      indicator-position-max = 1;
    };

    "org/gnome/shell/extensions/gsconnect" = {
      enabled = true;
      id = "c6d0043d-5ed1-4f37-a433-26014ab1ff4a";
      name = "deadbeef";
    };

    "org/gnome/shell/extensions/gtile" = {
      follow-cursor = false;
      grid-sizes = "8x6,8x4,6x4,4x4";
      theme = "Default";
    };

    "org/gnome/shell/extensions/just-perfection" = {
      accessibility-menu = true;
      activities-button = true;
      activities-button-icon-monochrome = true;
      aggregate-menu = true;
      app-menu = true;
      app-menu-icon = true;
      app-menu-label = true;
      background-menu = true;
      clock-menu = true;
      controls-manager-spacing-size = 0;
      dash = true;
      dash-icon-size = 0;
      double-super-to-appgrid = true;
      gesture = true;
      hot-corner = false;
      keyboard-layout = true;
      osd = true;
      panel = true;
      panel-arrow = true;
      panel-button-padding-size = 0;
      panel-corner-size = 0;
      panel-icon-size = 0;
      panel-in-overview = true;
      panel-indicator-padding-size = 0;
      panel-notification-icon = true;
      panel-size = 0;
      power-icon = true;
      ripple-box = true;
      search = false;
      show-apps-button = true;
      startup-status = 1;
      theme = false;
      window-demands-attention-focus = false;
      window-picker-icon = true;
      window-preview-caption = true;
      workspace = true;
      workspace-background-corner-size = 0;
      workspace-popup = true;
      workspace-switcher-should-show = false;
      workspaces-in-app-grid = true;
    };

    "org/gnome/shell/extensions/openweather" = {
      actual-city = 1;
      city = "46.0533463,11.4566004>Borgo Valsugana, Comunit224 Valsugana e Tesino, Provincia di Trento, Trentino-Alto Adige/S252dtirol, 38051, Italia>0 && 46.0405786,11.4652713>Olle, Borgo Valsugana, Comunit224 Valsugana e Tesino, Provincia di Trento, Trentino-Alto Adige/S252dtirol, 38051, Italia>0";
      position-in-panel = "right";
      show-comment-in-panel = true;
      translate-condition = true;
      use-system-icons = true;
    };

    "org/gnome/shell/extensions/pano" = {
      global-shortcut = [ "<Control><Alt>p" ];
      history-length = 1000;
      paste-on-select = false;
      play-audio-on-copy = false;
      send-notification-on-copy = false;
      shortcut = [ "<Control><Alt>p" ];
    };

    "org/gnome/shell/extensions/quick-settings-tweaks" = {
      list-buttons = "[{\"name\":\"SystemItem\",\"title\":null,\"visible\":true},{\"name\":\"OutputStreamSlider\",\"title\":null,\"visible\":true},{\"name\":\"InputStreamSlider\",\"title\":null,\"visible\":false},{\"name\":\"St_BoxLayout\",\"title\":null,\"visible\":true},{\"name\":\"BrightnessItem\",\"title\":null,\"visible\":true},{\"name\":\"NMWiredToggle\",\"title\":null,\"visible\":false},{\"name\":\"NMWirelessToggle\",\"title\":\"Wi-Fi\",\"visible\":true},{\"name\":\"NMModemToggle\",\"title\":null,\"visible\":false},{\"name\":\"NMBluetoothToggle\",\"title\":null,\"visible\":false},{\"name\":\"NMVpnToggle\",\"title\":null,\"visible\":false},{\"name\":\"BluetoothToggle\",\"title\":\"Bluetooth\",\"visible\":true},{\"name\":\"PowerProfilesToggle\",\"title\":\"Power Mode\",\"visible\":true},{\"name\":\"NightLightToggle\",\"title\":\"Night Light\",\"visible\":true},{\"name\":\"DarkModeToggle\",\"title\":\"Dark Style\",\"visible\":true},{\"name\":\"KeyboardBrightnessToggle\",\"title\":\"Keyboard\",\"visible\":true},{\"name\":\"RfkillToggle\",\"title\":\"Airplane Mode\",\"visible\":true},{\"name\":\"RotationToggle\",\"title\":\"Auto Rotate\",\"visible\":false},{\"name\":\"CaffeineToggle\",\"title\":\"Caffeine\",\"visible\":true},{\"name\":\"ServiceToggle\",\"title\":\"GSConnect\",\"visible\":true},{\"name\":\"DndQuickToggle\",\"title\":\"Do Not Disturb\",\"visible\":true},{\"name\":\"BackgroundAppsToggle\",\"title\":\"No Background Apps\",\"visible\":false},{\"name\":\"MediaSection\",\"title\":null,\"visible\":false},{\"name\":\"Notifications\",\"title\":null,\"visible\":false}]";
    };

    "org/gnome/shell/extensions/rounded-window-corners" = {
      custom-rounded-corner-settings = "@a{sv} {}";
      global-rounded-corner-settings = "{'padding': <{'left': <uint32 1>, 'right': <uint32 1>, 'top': <uint32 1>, 'bottom': <uint32 1>}>, 'keep_rounded_corners': <{'maximized': <false>, 'fullscreen': <false>}>, 'border_radius': <uint32 12>, 'smoothing': <uint32 0>}";
      settings-version = mkUint32 5;
    };

    "org/gnome/shell/extensions/trayIconsReloaded" = {
      applications = "[]";
      icon-margin-vertical = 4;
      icon-padding-horizontal = 16;
      icons-limit = 4;
      tray-margin-left = 4;
      tray-margin-right = 4;
    };

    "org/gnome/shell/extensions/vitals" = {
      hide-icons = false;
      hide-zeros = false;
      hot-sensors = [ "_memory_usage_" "__network-rx_max__" "_processor_usage_" ];
      icon-style = 1;
      include-static-gpu-info = true;
      menu-centered = true;
      network-speed-format = 1;
      show-battery = true;
      show-gpu = true;
    };

    "org/gnome/shell/weather" = {
      automatic-location = true;
      locations = "@av []";
    };

    "org/gnome/shell/world-clocks" = {
      locations = [ ];
    };

    "org/gnome/software" = {
      check-timestamp = mkInt64 1703844916;
      first-run = false;
      flatpak-purge-timestamp = mkInt64 1703847748;
    };

    "org/gnome/terminal/legacy/profiles:" = {
      list = [ "de8a9081-8352-4ce4-9519-5de655ad9361" "71a9971e-e829-43a9-9b2f-4565c855d664" "5083e06b-024e-46be-9cd2-892b814f1fc8" "95894cfd-82f7-430d-af6e-84d168bc34f5" ];
    };

    "org/gnome/terminal/legacy/profiles:/:5083e06b-024e-46be-9cd2-892b814f1fc8" = {
      background-color = "#24273a";
      foreground-color = "#cad3f5";
      highlight-background-color = "#24273a";
      highlight-colors-set = true;
      highlight-foreground-color = "#5b6078";
      palette = [ "#494d64" "#ed8796" "#a6da95" "#eed49f" "#8aadf4" "#f5bde6" "#8bd5ca" "#b8c0e0" "#5b6078" "#ed8796" "#a6da95" "#eed49f" "#8aadf4" "#f5bde6" "#8bd5ca" "#a5adcb" ];
      use-theme-colors = false;
      visible-name = "Catppuccin Macchiato";
    };

    "org/gnome/terminal/legacy/profiles:/:71a9971e-e829-43a9-9b2f-4565c855d664" = {
      background-color = "#303446";
      foreground-color = "#c6d0f5";
      highlight-background-color = "#303446";
      highlight-colors-set = true;
      highlight-foreground-color = "#626880";
      palette = [ "#51576d" "#e78284" "#a6d189" "#e5c890" "#8caaee" "#f4b8e4" "#81c8be" "#b5bfe2" "#626880" "#e78284" "#a6d189" "#e5c890" "#8caaee" "#f4b8e4" "#81c8be" "#a5adce" ];
      use-theme-colors = false;
      visible-name = "Catppuccin Frappe";
    };

    "org/gnome/terminal/legacy/profiles:/:95894cfd-82f7-430d-af6e-84d168bc34f5" = {
      background-color = "#1e1e2e";
      foreground-color = "#cdd6f4";
      highlight-background-color = "#1e1e2e";
      highlight-colors-set = true;
      highlight-foreground-color = "#585b70";
      palette = [ "#45475a" "#f38ba8" "#a6e3a1" "#f9e2af" "#89b4fa" "#f5c2e7" "#94e2d5" "#bac2de" "#585b70" "#f38ba8" "#a6e3a1" "#f9e2af" "#89b4fa" "#f5c2e7" "#94e2d5" "#a6adc8" ];
      use-theme-colors = false;
      visible-name = "Catppuccin Mocha";
    };

    "org/gnome/terminal/legacy/profiles:/:de8a9081-8352-4ce4-9519-5de655ad9361" = {
      background-color = "#eff1f5";
      foreground-color = "#4c4f69";
      highlight-background-color = "#eff1f5";
      highlight-colors-set = true;
      highlight-foreground-color = "#acb0be";
      palette = [ "#5c5f77" "#d20f39" "#40a02b" "#df8e1d" "#1e66f5" "#ea76cb" "#179299" "#acb0be" "#6c6f85" "#d20f39" "#40a02b" "#df8e1d" "#1e66f5" "#ea76cb" "#179299" "#bcc0cc" ];
      use-theme-colors = false;
      visible-name = "Catppuccin Latte";
    };

    "org/gnome/tweaks" = {
      show-extensions-notice = false;
    };

    "org/gtk/gtk4/settings/file-chooser" = {
      date-format = "regular";
      location-mode = "path-bar";
      show-hidden = false;
      show-size-column = true;
      show-type-column = true;
      sidebar-width = 140;
      sort-column = "name";
      sort-directories-first = false;
      sort-order = "ascending";
      type-format = "category";
      view-type = "list";
      window-size = mkTuple [ 859 374 ];
    };

    "org/gtk/settings/color-chooser" = {
      custom-colors = [ (mkTuple [ 1.0 0.596078 0.0 1.0 ]) (mkTuple [ 1.1765e-2 0.662745 0.956863 1.0 ]) (mkTuple [ 0.843137 0.588235 1.0 1.0 ]) ];
      selected-color = mkTuple [ true 1.0 0.596078 0.0 1.0 ];
    };

    "org/gtk/settings/file-chooser" = {
      date-format = "regular";
      location-mode = "path-bar";
      show-hidden = false;
      show-size-column = true;
      show-type-column = true;
      sidebar-width = 182;
      sort-column = "name";
      sort-directories-first = false;
      sort-order = "ascending";
      type-format = "category";
      window-position = mkTuple [ 0 32 ];
      window-size = mkTuple [ 1203 902 ];
    };

    "org/virt-manager/virt-manager" = {
      manager-window-height = 550;
      manager-window-width = 550;
    };

    "org/virt-manager/virt-manager/confirm" = {
      unapplied-dev = true;
    };

    "org/virt-manager/virt-manager/connections" = {
      autoconnect = [ "lxc:///" ];
      uris = [ "lxc:///" ];
    };

    "org/virt-manager/virt-manager/conns/lxc:" = {
      window-size = mkTuple [ 800 600 ];
    };

    "org/virt-manager/virt-manager/vmlist-fields" = {
      disk-usage = false;
      network-traffic = false;
    };

    "org/virt-manager/virt-manager/vms/879567cb333f4670ad5f1c0401288b65" = {
      autoconnect = 1;
      scaling = 1;
      vm-window-size = mkTuple [ 1024 810 ];
    };

  };
}
