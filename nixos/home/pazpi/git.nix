{ ... }:
let
  parameters = import ../../parameters.nix { };
in
{
  userName = "pazpi";
  userEmail = parameters.pazpiMail;
}
