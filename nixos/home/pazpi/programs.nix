{ pkgs, ... }:
{
  programs = {

    # alacritty = import ./alacritty.nix { };
    direnv = import ./direnv.nix { };
    kitty = import ./kitty.nix { };
    git = import ./git.nix { };
    vim = import ./vim.nix pkgs;
    vscode = import ./vscode.nix pkgs;
    # wezterm = import ./wezterm.nix { };
    zsh = import ./zsh.nix pkgs;
    firefox = import ./firefox.nix pkgs;
    ssh = import ./ssh.nix { };
  };
}
