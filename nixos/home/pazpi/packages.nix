{ pkgs, ... }:
{

  home.packages = with pkgs;
    [
      colmena
      devbox
      direnv
      esphome
      gitkraken
      jetbrains-toolbox
      nil
      nix-direnv
      nixpkgs-fmt
      remmina
      shellcheck
      sqlite
      yt-dlp
      zellij

      # gnome-extensions
      # gnomeExtensions.blur-my-shell
      # gnomeExtensions.caffeine
      # gnomeExtensions.dash-to-dock
      # gnomeExtensions.gsconnect
      # gnomeExtensions.gtile
      # gnomeExtensions.just-perfection
      # gnomeExtensions.pano
      # gnomeExtensions.quick-settings-tweaker
      # gnomeExtensions.rounded-window-corners
      # gnomeExtensions.tailscale-status
      # gnomeExtensions.vitals
    ];
}
