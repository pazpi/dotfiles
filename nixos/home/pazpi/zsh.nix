{ pkgs, ... }:
{
  autosuggestion.enable = true;
  enableCompletion = true;

  # syntaxHighlighting = {
  #   enable = true;
  # };

  # Automatically enter into a directory if typed directly into shell
  autocd = true;

  defaultKeymap = "viins";

  history = {
    expireDuplicatesFirst = true;
    ignoreDups = true;
    save = 100000000;
    size = 1000000000;
  };

  shellAliases = {
    l = "lsd -lh";
    la = "lsd -lha";
    df = "df -h";
    free = "free -m";
    grep = "rg";
    ssh-tunnel = "ssh -D 5000 -N pazpi.top";
    weather = "curl wttr.in";
    youtube-max = "youtube-dl -f bestvideo+bestaudio ";
    youtube-wl = "mpv 'https://www.youtube.com/playlist?list=WL' --ytdl-raw-options=playlist-reverse=";
  };

  localVariables = {
    FZF_DEFAULT_COMMAND = "fd --type f --strip-cwd-prefix --hidden --follow --exclude .git";
    FZF_CTRL_T_OPTS = "--preview 'bat -n --color=always {}' --bind 'ctrl-/:change-preview-window(down|hidden|)'";
    FZF_ALT_C_OPTS = "--preview 'tree -C {}'";
  };

  prezto = {
    # TODO: command error on shell close with Gnome-Console
    enable = true;
    caseSensitive = true;
  };

  zplug = {
    plugins = [
      { name = "chisui/zsh-nix-shell"; }
      { name = "modules/git"; tags = [ "from:prezto" ]; }
      { name = "zsh-users/zsh-autosuggestions"; }
    ];
  };
}
