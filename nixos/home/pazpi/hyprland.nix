{ ... }:
{
  wayland.windowManager.hyprland = {
    enable = true;
    enableNvidiaPatches = true;

    settings = {
      "$mod" = "SUPER";
      "$terminal" = "wezterm";
      bind =
        [
          "$mod, F, exec, firefox"
          ", Print, exec, grimblast copy area"
        ];
    };

    plugins = [ ];

    extraConfig = '''';
  };
}
