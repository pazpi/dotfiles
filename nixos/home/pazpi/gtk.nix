{ config, pkgs, ... }:
{
  gtk = {

    enable = true;

    # cursorTheme = {
    #   package = pkgs.catppuccin-cursors.macchiatoDark;
    #   name = "Catppuccin-Macchiato-Dark-Cursors";
    #   size = 32;
    # };

    theme = {
      name = "Catppuccin-Macchiato-Compact-Pink-Dark";
      package = pkgs.catppuccin-gtk.override {
        accents = [ "green" ];
        size = "compact";
        tweaks = [ "rimless" "black" ];
        variant = "latte";
      };
    };

    gtk3.extraConfig = {
      Settings = ''
        gtk-application-prefer-dark-theme=0
        gtk-theme-name=Catppuccin-Macchiato-Compact-Pink-Dark
      '';
    };

    gtk4.extraConfig = {
      Settings = ''
        gtk-application-prefer-dark-theme=0
        gtk-theme-name=Catppuccin-Macchiato-Compact-Pink-Dark
      '';
    };

  };

  xdg.configFile = {
    "gtk-4.0/assets".source = "${config.gtk.theme.package}/share/themes/${config.gtk.theme.name}/gtk-4.0/assets";
    "gtk-4.0/gtk.css".source = "${config.gtk.theme.package}/share/themes/${config.gtk.theme.name}/gtk-4.0/gtk.css";
    "gtk-4.0/gtk-dark.css".source = "${config.gtk.theme.package}/share/themes/${config.gtk.theme.name}/gtk-4.0/gtk-dark.css";
  };

}
