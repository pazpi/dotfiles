{ pkgs, ... }:
{
  enable = true;

  package = pkgs.firefox.override
    {
      # See nixpkgs' firefox/wrapper.nix to check which options you can use
      nativeMessagingHosts = [
        pkgs.kdePackages.plasma-browser-integration
      ];
    };

  profiles.default = {
    name = "Default";
    settings = {
      # Pazpi config
      "browser.backspace_action" = 0; # Backspace previous tab
      "browser.translation.neverForLanguages" = "it";
      "browser.translations.neverTranslateLanguages" = "it";

      "extensions.activeThemeID" = "firefox-compact@mozilla.org";

      # For Firefox GNOME theme:
      # "toolkit.legacyUserProfileCustomizations.stylesheets" = true;
      # "browser.tabs.drawInTitlebar" = true;
      # "svg.context-properties.content.enabled" = true;
    };
    # userChrome = ''
    #   @import "firefox-gnome-theme/userChrome.css";
    #   @import "firefox-gnome-theme/theme/colors/dark.css";
    # '';
  };

}
