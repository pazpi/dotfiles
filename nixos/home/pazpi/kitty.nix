{ ... }:
{
  enable = true;
  font.name = "Iosevka";

  keybindings = {
    "alt+c" = "copy_to_clipboard";
    "alt+v" = "paste_from_clipboard";
    "kitty_mod+enter" = "new_window_with_cwd";
    "kitty_mod+left" = "previous_window";
    "kitty_mod+right" = "next_window";
    "kitty_mod+shift+left" = "next_tab";
    "kitty_mod+shift+right" = "previous_tab";
    "kitty_mod+shift+r" = "new_window ranger";
    "kitty_mod+shift+t" = "set_tab_title";
    "kitty_mod+q" = "close_tab";
    "kitty_mod+t" = "new_tab";
  };

  settings = {
    # kitty_mod = "ctrl+shift";
    kitty_mod = "alt";
    scrollback_lines = 10000;
    enable_audio_bell = false;
    update_check_interval = 0;
    copy_on_select = true;
    draw_minimal_borders = true;
  };

  shellIntegration.enableZshIntegration = true;

  theme = "Catppuccin-Macchiato";
}
