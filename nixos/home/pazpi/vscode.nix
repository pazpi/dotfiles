{ pkgs, ... }:
let
  extensions = import ../../Dependecies/vscode.extensions.nix;
in
{
  enable = true;
  enableUpdateCheck = false;

  extensions = with extensions.vscode-marketplace; [
    # Bash
    timonwong.shellcheck

    # C#
    (ms-dotnettools.csdevkit.overrideAttrs (_: { sourceRoot = "./extension"; }))
    (ms-dotnettools.vscodeintellicode-csharp.overrideAttrs (_: { sourceRoot = "./extension"; }))
    adrianwilczynski.namespace
    fernandoescolar.vscode-solution-explorer
    fireside21.cshtml
    k--kato.docomment
    ms-dotnettools.csharp
    ms-dotnettools.vscode-dotnet-runtime
    patcx.vscode-nuget-gallery

    # Nix
    arrterian.nix-env-selector
    jnoortheen.nix-ide
    pinage404.nix-extension-pack

    # Theme
    catppuccin.catppuccin-vsc-icons
    raillyhugo.one-hunter

    # Formatter
    redhat.vscode-xml

    # Markdown
    yzhang.markdown-all-in-one

    # Tool
    alexcvzz.vscode-sqlite
    jetpack-io.devbox
    mkhl.direnv
    ms-vscode-remote.remote-ssh
    ms-vscode-remote.remote-ssh-edit
    ms-vscode.remote-explorer
    rangav.vscode-thunder-client
  ];

  keybindings = [
    {
      "key" = "ctrl+k ctrl+c";
      "command" = "editor.action.commentLine";
      "when" = "editorTextFocus && !editorReadonly";
    }
    {
      "key" = "ctrl+k ctrl+u";
      "command" = "editor.action.removeCommentLine";
      "when" = "editorTextFocus && !editorReadonly";
    }
    {
      "key" = "ctrl+r ctrl+r";
      "command" = "editor.action.rename";
      "when" = "editorHasRenameProvider && editorTextFocus && !editorReadonly";
    }
    {
      "key" = "alt+d";
      "command" = "editor.action.copyLinesDownAction";
      "when" = "editorTextFocus && !editorReadonly";
    }
    {
      "key" = "ctrl+shift+alt+down";
      "command" = "-editor.action.copyLinesDownAction";
      "when" = "editorTextFocus && !editorReadonly";
    }
    {
      "key" = "shift+alt+d";
      "command" = "editor.action.copyLinesUpAction";
      "when" = "editorTextFocus && !editorReadonly";
    }
    {
      "key" = "ctrl+shift+alt+up";
      "command" = "-editor.action.copyLinesUpAction";
      "when" = "editorTextFocus && !editorReadonly";
    }
  ];

  userSettings = {
    # Editor
    "telemetry.telemetryLevel" = "off";
    "update.mode" = "none";

    "workbench.colorTheme" = "Flexoki Light";
    "workbench.iconTheme" = "catppuccin-latte";
    "workbench.editor.wrapTabs" = true;
    "explorer.excludeGitIgnore" = true;
    "files.autoSave" = "afterDelay";
    "files.autoSaveDelay" = 5000;
    "diffEditor.codeLens" = true;
    "diffEditor.ignoreTrimWhitespace" = false;
    "editor.bracketPairColorization.enabled" = true;
    "editor.formatOnPaste" = true;
    "editor.formatOnSave" = true;
    "editor.formatOnType" = true;
    "editor.quickSuggestions" = {
      "comments" = "on";
      "strings" = "on";
      "other" = "on";
    };

    "window.titleBarStyle" = "custom";

    # Git
    "git.enableSmartCommit" = true;
    "git.autofetch" = true;

    # C#
    # "csharp.maxProjectFileCountForDiagnosticAnalysis" = 100;
    # "csharp.semanticHighlighting.enabled" = true;
    # "csharp.suppressHiddenDiagnostics" = false;
    # "dotnetAcquisitionExtension.enableTelemetry" = false;
    # "omnisharp.enableEditorConfigSupport" = true;
    # "omnisharp.useModernNet" = true;
    # "omnisharp.useGlobalMono" = "never";
    #"dotnet.dotnetPath" = "/home/${config.home.username}/.nix-profile/bin/dotnet";
    #"omnisharp.sdkPath" = "/home/${config.home.username}/.nix-profile/bin/dotnet";
    #"dotnet.preferCSharpExtension" = false;
    # "dotnetAcquisitionExtension.existingDotnetPath" = [
    #   {
    #     "extensionId" = "ms-dotnettools.csdevkit";
    #     "path" = "/home/${config.home.username}/.nix-profile/bin/dotnet";
    #   }
    # ];
    # "NugetGallery.sources" = [
    #   "{\"name\": \"nuget.org\",\"url\": \"https://api.nuget.org/v3/index.json\"}"
    # ];

    # Markdown
    "markdown-preview-enhanced.previewTheme" = "github-dark.css";

    # Python
    "python.experiments.enabled" = false;
    "python.analysis.inlayHints.variableTypes" = true;
    "python.analysis.inlayHints.pytestParameters" = true;
    "python.analysis.inlayHints.functionReturnTypes" = true;

    # Bash
    "shellcheck.customArgs" = [ "-x" ];
    "shellcheck.exclude" = [
      "2001"
    ];

    # Nix
    "nix.formatterPath" = [ "nix" "fmt" "--" "-" ];
    "nix.serverPath" = "nil";
    "nix.enableLanguageServer" = true;
    "nix.serverSettings" = {
      "nil" = {
        "diagnostics" = {
          "ignored" = [ "unused_binding" "unused_with" ];
        };
        "formatting" = {
          "command" = [ "nixpkgs-fmt" ];
        };
      };
    };
  };


}
