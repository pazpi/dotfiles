{ pkgs, ... }:
let
  parameters = import ./parameters.nix { };
in
{

  users = {
    groups = {
      users.gid = 100;
      plocate.gid = 200;
    };

    users = {

      root = {
        uid = 0;
      };

      pazpi = {
        uid = 1000;
        isNormalUser = true;
        isSystemUser = false;
        shell = pkgs.zsh;
        description = "Davide Pasetto";
        extraGroups = [ "users" "wheel" "libvirtd" "lxd" "dialout" ];
        initialHashedPassword = parameters.pazpiPwd;
      };

    };
  };

}
