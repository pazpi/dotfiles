# (import
#   (builtins.fetchGit "https://github.com/nix-community/nix-vscode-extensions")
# ).extensions.${builtins.currentSystem}


(import (builtins.fetchGit {
  url = "https://github.com/nix-community/nix-vscode-extensions";
  ref = "refs/heads/master";
  rev = "c43d9089df96cf8aca157762ed0e2ddca9fcd71e";
})).extensions.${builtins.currentSystem}
