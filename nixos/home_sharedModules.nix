{ pkgs, ... }:
let
  parameters = import ./parameters.nix { };
in
{
  home-manager.sharedModules = [{

    fonts.fontconfig.enable = true;

    home = {
      stateVersion = parameters.nixRelease;

      packages = with pkgs; [
        fzf
        lsd
        starship # prompt
        (pkgs.nerdfonts.override { fonts = [ "FiraCode" "Iosevka" ]; })
      ];

      # Common shell aliases
      shellAliases = {
        nvidia-run = "__NV_PRIME_RENDER_OFFLOAD=1 __GLX_VENDOR_LIBRARY_NAME=nvidia";
        update = "sudo nixos-rebuild switch";
        update-test = "sudo nixos-rebuild test";
        update-build-vm = "sudo nixos-rebuild build-vm";
      };
    };

    programs = {
      command-not-found.enable = true;

      git = {
        enable = true;
        extraConfig = {
          push = { default = "simple"; };
          pager = {
            branch = false;
            log = false;
            status = false;
            diff = false;
          };
        };
      };

      # Let Home Manager install and manage itself.
      home-manager.enable = true;

      vim = {
        enable = true;
      };

      zsh = {
        enable = true;
        initExtra = builtins.readFile config/zsh_init.txt;
        zplug = {
          enable = true;
        };
      };

    };
  }];
}
