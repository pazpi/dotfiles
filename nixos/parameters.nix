{}:
{
  hostname = "deadbeef";
  pazpiPwd = "$y$j9T$dA94KVg1/jYLqclQQbTDk.$cnfxBWUN8P4shr8Kkipv5bU/RCtQNoAwYFDZ0X/BYs5";
  pazpiMail = "pasettodavide@gmail.com";
  nixRelease = "23.05"; # relase version of the first install of this system
  currentNixRelease = "24.05";
}
