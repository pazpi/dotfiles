{ config, lib, pkgs, ... }:
{

  boot = {
    kernel = {
      sysctl."fs.inotify.max_user_instances" = lib.mkDefault 524288;
      sysctl."fs.inotify.max_user_watches" = lib.mkDefault 524288;
    };

    plymouth = {
      enable = true;
      theme = "colorful_loop";
      themePackages = [ (pkgs.adi1090x-plymouth-themes.override { selected_themes = [ "colorful_loop" ]; }) ];
    };

    loader = {

      # Don't use the systemd-boot EFI boot loader.
      # Necessary for dual boot Windows
      systemd-boot.enable = false;

      efi = {
        canTouchEfiVariables = true;
        efiSysMountPoint = "/boot";
      };
      grub = {
        enable = true;
        devices = [ "nodev" ];
        efiSupport = true;
        extraEntries = ''
          menuentry "Windows" {
            insmod part_gpt
              insmod fat
              insmod search_fs_uuid
              insmod chain
              search --fs-uuid --set=root FE74-E293
              chainloader /EFI/Microsoft/Boot/bootmgfw.efi
          }
        '';
      };
    };
  };

}
