# Pazpi configuration on XPS 15 9560

# Remember to add the unstable channel for some packages:
# sudo nix-channel --add https://nixos.org/channels/nixpkgs-unstable nixpkgs-unstable

{ config, pkgs, ... }:
let
  parameters = import ./parameters.nix { };
  nixos-hardware = builtins.fetchGit "https://github.com/NixOS/nixos-hardware.git";
in
{
  imports =
    [
      # Include the results of the hardware scan.
      ./hardware-configuration.nix

      # NixOS on XPS 15 9560
      (import "${nixos-hardware}/dell/xps/15-9560")

      # Test config with build-vm
      ./build_vm.nix

      # Network settings
      ./network.nix

      # Boot settings
      ./boot.nix

      # Enabled Services, Gnome, ecc
      ./services.nix

      # Include the package list
      ./packages.nix

      # Virtualization and LXC
      ./lxc.nix

      # Handle local users
      ./users.nix

      # Home Manager
      ./home.nix
    ];

  # Set your time zone.
  time.timeZone = "Europe/Rome";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "it_IT.UTF-8";
    LC_IDENTIFICATION = "it_IT.UTF-8";
    LC_MEASUREMENT = "it_IT.UTF-8";
    LC_MONETARY = "it_IT.UTF-8";
    LC_NAME = "it_IT.UTF-8";
    LC_NUMERIC = "it_IT.UTF-8";
    LC_PAPER = "it_IT.UTF-8";
    LC_TELEPHONE = "it_IT.UTF-8";
    LC_TIME = "it_IT.UTF-8";
  };

  # Experimental feature
  nix = {
    gc = {
      automatic = true; # Enable the automatic garbage collector
      dates = "weekly"; # When to run the garbage collector
      options = "-d"; # Arguments to pass to nix-collect-garbage
    };
    settings.experimental-features = [ "nix-command" "flakes" ];
  };

  # Configure console keymap
  console.keyMap = "it";

  # Enable sound with pipewire.
  sound.enable = true;

  hardware = {
    pulseaudio.enable = false;

    # Necessary for Steam
    opengl.driSupport32Bit = true;

    bluetooth = {
      enable = true;
      powerOnBoot = true;
    };

    # Mouse logitech
    logitech.wireless = {
      enable = true;
      enableGraphical = true;
    };
  };

  security.rtkit.enable = true;

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  system.copySystemConfiguration = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It's perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = parameters.nixRelease; # Did you read the comment?

}
