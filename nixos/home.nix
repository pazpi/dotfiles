{ config, lib, pkgs, modulesPath, ... }:
let
  home-manager = builtins.fetchTarball "https://github.com/nix-community/home-manager/archive/release-${parameters.currentNixRelease}.tar.gz";

  parameters = import ./parameters.nix { };
in

{
  imports = [
    (import "${home-manager}/nixos")
    ./home_sharedModules.nix # Generic options for all users
  ];

  home-manager = {

    useGlobalPkgs = true;
    useUserPackages = true;

    backupFileExtension = "backup";

    users = {
      pazpi = {

        # This value determines the Home Manager release that your
        # configuration is compatible with. This helps avoid breakage
        # when a new Home Manager release introduces backwards
        # incompatible changes.
        #
        # You can update Home Manager without changing this value. See
        # the Home Manager release notes for a list of state version
        # changes in each release.
        home.stateVersion = parameters.nixRelease;

        # home.pointerCursor = {
        #   gtk.enable = true;
        #   size = 32;
        # };

        manual.html.enable = false;
        manual.manpages.enable = false;

        # Add Firefox GNOME theme directory
        # home.file."firefox-gnome-theme" = {
        #   target = ".mozilla/firefox/default/chrome/firefox-gnome-theme";
        #   source = (fetchTarball "https://github.com/rafaelmardojai/firefox-gnome-theme/archive/master.tar.gz");
        # };

        imports = [
          # ./home/pazpi/dconf.nix
          # ./home/pazpi/gtk.nix
          ./home/pazpi/qt.nix
          # ./home/pazpi/hyprland.nix
          ./home/pazpi/programs.nix
          ./home/pazpi/packages.nix
        ];

        services.kdeconnect.enable = true;

      };
    };
  };
}
