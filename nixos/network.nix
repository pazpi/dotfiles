{ config, ... }:
let
  parameters = import ./parameters.nix { };
in
{
  networking = {
    hostName = parameters.hostname;

    # wireless.interfaces = [ "wlp10s0" ];
    # wireless.enable = true;  # Enables wireless support via wpa_supplicant.

    # Configure network proxy if necessary
    # proxy.default = "http://user:password@proxy:port/";
    # proxy.noProxy = "127.0.0.1,localhost,internal.domain";

    # Enable networking
    networkmanager = {
      enable = true;
      wifi.powersave = false;
    };

    nameservers = [
      "100.100.100.100"
      "192.168.1.2"
      "1.1.1.1"
      "8.8.8.8"
    ];


    # Open ports in the firewall.
    # firewall.allowedTCPPorts = [ ... ];
    # firewall.allowedUDPPorts = [ ... ];
    # Or disable the firewall altogether.
    # firewall.enable = false;

  };
}
