#!/usr/bin/env python3
#
# Copyright (C) 2016 James Murphy
# Licensed under the GPL version 2 only
#
# A battery indicator blocklet script for i3blocks

import os
from subprocess import check_output,call

status = check_output(['acpi'], universal_newlines=True)

if not status:
    # stands for no battery found
    fulltext = "<span color='red'><span font='FontAwesome'>\uf00d \uf240</span></span>"
    percentleft = 100
else:
    state = status.split(": ")[1].split(", ")[0]
    commasplitstatus = status.split(", ")
    percentleft = int(commasplitstatus[1].rstrip("%\n"))

    # battery icon when discharging
    FA_DIS_EMPTY = "<span color='red'><span font='FontAwesome'>\uf244</span></span> "
    FA_DIS_QUARTER = "<span color='yellow'><span font='FontAwesome'>\uf243</span></span> "
    FA_DIS_HALF = "<span color='white'><span font='FontAwesome'>\uf242</span></span> "
    FA_DIS_T_QUARTER = "<span color='white'><span font='FontAwesome'>\uf241</span></span> "
    FA_DIS_FULL = "<span color='green'><span font='FontAwesome'>\uf240</span></span> "

    # stands for charging
    FA_LIGHTNING = "<span color='yellow'><span font='FontAwesome'>\uf0e7</span></span>"

    # stands for plugged in
    FA_PLUG = "<span font='FontAwesome'>\uf1e6</span>"

    # battery unknow
    FA_UNKNOW = "<span font='FontAwesome'>\uf128</span> "

    fulltext = ""
    timeleft = ""

    if state == "Discharging":
        time = commasplitstatus[-1].split()[0]
        time = ":".join(time.split(":")[0:2])
        timeleft = " ({})".format(time)
    elif state == "Full":
        fulltext = FA_PLUG + " "
    elif state == "Unknown":
        fulltext = "<span font='FontAwesome'>\uf128</span> "
    else:
        fulltext = FA_LIGHTNING + " " + FA_PLUG + " "

    def color(percent):
        if percent < 10:
            # exit code 33 will turn background red
            return "#FFFFFF"
        if percent < 20:
            return "#FF3300"
        if percent < 30:
            return "#FF6600"
        if percent < 40:
            return "#FF9900"
        if percent < 50:
            return "#FFCC00"
        if percent < 60:
            return "#FFFF00"
        if percent < 70:
            return "#FFFF33"
        if percent < 80:
            return "#FFFF66"
        return "#FFFFFF"

    def discharge_icon(percent):
        if percent < 10:
            return FA_DIS_EMPTY + str(percent)
        if percent < 20:
            return FA_DIS_QUARTER + str(percent)
        if percent < 30:
            return FA_DIS_QUARTER + str(percent)
        if percent < 40:
            return FA_DIS_HALF + str(percent)
        if percent < 50:
            return FA_DIS_HALF + str(percent)
        if percent < 60:
            return FA_DIS_HALF + str(percent)
        if percent < 70:
            return FA_DIS_T_QUARTER + str(percent)
        if percent < 80:
            return FA_DIS_T_QUARTER + str(percent)
        if percent < 90:
            return FA_DIS_FULL + str(percent)
        return " "

    form =  '<span color="{}">{}%</span>'
    fulltext += form.format(color(percentleft), discharge_icon(percentleft))
    fulltext += timeleft

print(fulltext)
print(fulltext)
if percentleft < 10:
    exit(33)
