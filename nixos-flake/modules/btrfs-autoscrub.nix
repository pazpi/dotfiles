{ lib, config, pkgs, ...}:
let
    cfg = config.btrfs-autoscrub;
in
{
    options.btrfs-autoscrub = {
        enable = lib.mkEnableOption "Enable BTRFS Auto Scrub module";
        interval = lib.mkOption {
            default = "weekly";
            description = config.services.btrfs.autoScrub.interval.description;
        };
        
        fileSystems = lib.mkOption {
            default = [ ]
            description = config.services.btrfs.autoScrub.fileSystems.description;
        }

    };

    config = lib.mkIf cfg.enable {
        btrfs.autoScrub = {
      enable = true;
      interval = cfg.interval;
      fileSystems = cfg.interval;
    };
    };

}