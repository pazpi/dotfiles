{ lib, config, pkgs, ...}:
let
    cfg = config.sound;
in
{
    options.sound = {
        enable = lib.mkEnableOption "Enable sound module";
    };

    config = lib.mkIf cfg.enable {
        # Enable sound with pipewire.
        sound.enable = true;
        pipewire = {
            enable = true;
            alsa.enable = true;
            alsa.support32Bit = true;
            pulse.enable = true;
        };

    };

}