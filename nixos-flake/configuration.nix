# Pazpi configuration on XPS 15 9560

# Remember to add the unstable channel for some packages:
# sudo nix-channel --add https://nixos.org/channels/nixpkgs-unstable nixpkgs-unstable

{ config, pkgs, ... }:
{
  imports =
    [
      # Include the results of the hardware scan.
      ./hardware-configuration.nix

      # NixOS on XPS 15 9560
      (import "${nixos-hardware}/dell/xps/15-9560")

      # Test config with build-vm
      ./build_vm.nix

      # Network settings
      ./network.nix

      # Boot settings
      ./boot.nix

      # Enabled Services, Gnome, ecc
      ./services.nix

      # Include the package list
      ./packages.nix

      # Handle local users
      ./users.nix

      # Home Manager
      ./home.nix
    ];

  # Set your time zone.
  time.timeZone = "Europe/Rome";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "it_IT.UTF-8";
    LC_IDENTIFICATION = "it_IT.UTF-8";
    LC_MEASUREMENT = "it_IT.UTF-8";
    LC_MONETARY = "it_IT.UTF-8";
    LC_NAME = "it_IT.UTF-8";
    LC_NUMERIC = "it_IT.UTF-8";
    LC_PAPER = "it_IT.UTF-8";
    LC_TELEPHONE = "it_IT.UTF-8";
    LC_TIME = "it_IT.UTF-8";
  };


  # Configure console keymap
  console.keyMap = "it2";

  # Enable sound with pipewire.
  sound.enable = true;

  hardware.pulseaudio.enable = false;

  # Necessary for Steam
  hardware.opengl.driSupport32Bit = true;

  # Mouse logitech
  hardware.logitech.wireless = {
    enable = true;
    enableGraphical = true;
  };

  security.rtkit.enable = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    vimPlugins.vim-clap
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  system.copySystemConfiguration = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It's perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = parameters.nixRelease; # Did you read the comment?

}
