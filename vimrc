" vimrc by @pazpi
"
" vim-polyglot ----------------------------------------------
let g:ale_disable_lsp = 0
let g:polyglot_disabled = ['csv']
"'-----------------------------------------------------------
"
" Plug {{{
" fix for base16 color in vim 8.1
function FixupBase16(info)
    !sed -i '/Base16hi/\! s/a:\(attr\|guisp\)/l:\1/g' ~/.vim/plugged/base16-vim/colors/*.vim
endfunction
" curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
"    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
call plug#begin('~/.vim/plugged')
	" Vim Enhancements
    Plug 'christoomey/vim-tmux-navigator'		" use C-{hjkl} for vim buffer and tmux split panels
    Plug 'AndrewRadev/switch.vim'				" switch between two world (TRUE<->FALSE)
    Plug 'junegunn/vim-easy-align', { 'on': ['<Plug>(EasyAlign)', 'EasyAlign'] }	" allign text
    Plug 'machakann/vim-highlightedyank'		" see what you have yanked
    Plug 'itchyny/calendar.vim'					" calen inside vim

	" Vim Functional Enchancements
    Plug 'scrooloose/nerdcommenter'				" quick comment line of code
	Plug 'w0rp/ale'								" error checking multiple language
    Plug 'majutsushi/tagbar'					" left panel with tag (es function or macro for different programming language
    Plug 'liuchengxu/vista.vim'					" Ctags LSP da capire come funzione
	Plug 'Shougo/deoplete.nvim'
	Plug 'roxma/nvim-yarp'
	Plug 'roxma/vim-hug-neovim-rpc'
	Plug 'inkarkat/vim-ingo-library'
	Plug 'vim-scripts/AdvancedSorters'

	" GUI enhancements
	Plug 'vim-airline/vim-airline'				" status line
	Plug 'vim-airline/vim-airline-themes'		" theme for airline
	Plug 'mattboehm/vim-accordion'				" shirinks other splits beside the current one :Accordiion 3
	Plug 'mattboehm/vim-unstack'				" parse Python stack traces

	" Git
	Plug 'airblade/vim-gitgutter'				" git diff next to line number
	Plug 'tpope/vim-fugitive'					" manage git inside vim
	Plug 'junegunn/gv.vim'						" git tree :GV

	" Semantic language support
	" Plug 'neoclide/coc.nvim', {'branch': 'release'}

	" Syntactic language support
	Plug 'rust-lang/rust.vim'					" Rust syntax support
	Plug 'cespare/vim-toml'
	Plug 'sheerun/vim-polyglot'					" syntax indent and ftplugin for multiple language
	Plug 'rhysd/vim-clang-format'				" C/C++/TypeScript and other (check docs)

	" Fuzzy Finders
    Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }				" fuzzy finder
    Plug 'junegunn/fzf.vim'
	Plug 'liuchengxu/vim-clap', { 'do': ':Clap install-binary' }

	" Colorschema
	Plug 'morhetz/gruvbox'
	Plug 'Rigellute/rigel'
	Plug 'deoplete-plugins/deoplete-jedi'
	Plug 'rakr/vim-one'
call plug#end()
" }}}
"
" Abbreviation {{{
iabbrev @@ pasettodavide@gmail.com
" }}}
"
" General Behavior {{{
syntax enable

" change leader to ,
let mapleader=","
let mapocalleader="\\"
set updatetime=700	" length of time Vim waits after you stop typing before it triggers the plugin is governed by the setting updatetime
set spellfile=~/vim/spell/en.utf-8.add

" Add all subdirectory to vim path
set path+=**
set path+=/usr/local/include/**
set wildignore+=**/.git/**
set wildignore+=**/venv/**
" }}}

" Visual modification {{{
set background=dark
set t_Co=256					" 256 color support
" set cursorline                " highlight current line
if &term =~ '256color'
    " Disable Background Color Erase (BCE) so that color schemes
    " work properly when Vim is used inside tmux and GNU screen.
    set t_ut=
endif
"
if has("termguicolors")
	set termguicolors
endif
"
" Task tags
if has("autocmd")
  " Highlight TODO, FIXME, NOTE, etc.
  if v:version > 701
    autocmd Syntax * call matchadd('Todo',  '\W\zs\(TODO\|FIXME\|CHANGED\|XXX\|BUG\|HACK\)')
    autocmd Syntax * call matchadd('Debug', '\W\zs\(NOTE\|INFO\|IDEA\)')
  endif
endif

if executable('rg')
	set grepprg=rg\ --no-heading\ --vimgrep
	set grepformat=%f:%l:%c:%m
endif

colorscheme gruvbox

" }}}
"
" Editor layout {{{
set termencoding=utf-8
set encoding=utf-8
set lazyredraw                  " don't update the display while executing macros
set laststatus=2                " tell VIM to always put a status line in, even if there is only one window
set cmdheight=2                 " use a status bar that is 2 rows high

set printfont=:h10
set printencoding=utf-8
set printoptions=paper:letter
" Always draw sign column. Prevent buffer moving when adding/deleting sign.
set signcolumn=yes

" }}}
"
" Editing behavior {{{
set showmode                   " always show what mode we're currently editing in
set wrap                       " wrap lines
set tabstop=4                  " a tab is four spaces
set softtabstop=4              " when hitting <BS>, pretend like a tab is removed, even if spaces
set shiftwidth=4               " number of spaces to use for autoindenting
set shiftround                 " use multiple of shiftwidth when indenting with '<' and '>'
set autoindent                 " always set autoindenting on
set copyindent                 " copy the previous indentation on autoindenting
set number relativenumber      " always show line numbers
highlight LineNr ctermfg=white " white line number
set showmatch                  " set show matching parenthesis
set ignorecase                 " ignore case when searching
set smartcase                  " ignore case if search pattern is all lowercase, case-sensitive otherwise
set smarttab                   " insert tabs on the start of a line according to shiftwidth, not tabstop
set scrolloff=4                " keep 4 lines off the edges of the screen when scrolling
set virtualedit=block          " allow the cursor to go in to "invalid" places
set hlsearch                   " highlight search terms
set incsearch                  " show search matches as you type
set gdefault                   " search/replace "globally" (on a line) by default
set pastetoggle=<F2>           " when in insert mode, press <F2> to go to paste mode, where you can paste mass data that won't be autoindented
set mouse=a                     " enable using the mouse if terminal emulator supports it (xterm does)
set fileformats="unix,dos,mac" " set EOF formats
set formatoptions+=1           " When wrapping paragraphs, don't end lines with 1-letter words (looks stupid)
set nrformats=                 " make <C-a> and <C-x> play well with zero-padded numbers (i.e. don't consider them octal or hex)
set clipboard=unnamedplus      " normal OS clipboard interaction
set foldenable
set foldmethod=syntax          " folding method based on the syntax
set foldlevel=99               " set folding to 99 aka never fold by default
set whichwrap+=<,>,h,l,[,]     " move up/down when reaching end of current line
set indentkeys-=:
" }}}
"
" Vim behaviour {{{
set backspace=2                      " Backspace over newlines
set hidden                           " hide buffers instead of closing them this means that the current buffer can be put to background without being written; and that marks and undo history are preserved
set switchbuf=useopen                " reveal already opened files from the quickfix window instead of opening new buffers
set history=3000                     " remember more commands and search history
set undolevels=3000                  " use many muchos levels of undo
set noeb vb t_vb=                    " Disable flashing
if v:version >= 730
    set undofile                     " keep a persistent backup file
    set undodir=~/.vim/undo,~/tmp,/tmp
endif
set nobackup                         " do not keep backup files, it's 70's style cluttering
set noswapfile                       " do not write annoying intermediate swap files
set directory=~/.vim/.tmp,~/tmp,/tmp " store swap files in one of these directories (in case swapfile is ever turned on)

" Decent wildmenu
set wildmenu                    " make tab completion for files/buffers act like bash
set wildmode=longest:full,full			" show a list when pressing tab and complete first full match
set wildignore=*.swp,*.bak,*.pyc,*.class,.hg,.svn,*~,*.png,*.jpg,*.gif

set title                       " change the terminal's title
set titleold=					" restore previous terminal title
set showcmd                     " show (partial) command in the last line of the screen this also shows visual selection info
set nomodeline                  " disable mode lines (security measure) (read and execute first and last line of file)
autocmd FileType help wincmd L  " open :help on vertical split
"Jump to the last position when reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif
" }}}
"
" Key re-map {{{
"
" Copy and paste from system clipboard
inoremap <C-v> <ESC>"+pa
vnoremap <C-c> "+y
vnoremap <C-d> "+d
"
" Toggle show/hide invisible chars
nnoremap <leader>i :set list!<cr>
" \v very magical, thread special ASCII character as command (es $ end of line)
nnoremap / /\v
vnoremap / /\v
"
" Speed up scrolling of the viewport slightly
nnoremap <C-e> 4<C-e>
nnoremap <C-y> 4<C-y>
"
" Mappings to easily toggle fold levels
nnoremap z0 :set foldlevel=0<cr>
nnoremap z1 :set foldlevel=1<cr>
nnoremap z2 :set foldlevel=2<cr>
nnoremap z3 :set foldlevel=3<cr>
nnoremap z4 :set foldlevel=4<cr>
nnoremap z5 :set foldlevel=5<cr>
nnoremap <Space> za
vnoremap <Space> za
"
" Use Q for formatting the current paragraph (or visual selection)
vnoremap Q gq
nnoremap Q gqap
"
" make p in Visual mode replace the selected text with the yank register
vnoremap p <Esc>:let current_reg = @"<CR>gvdi<C-R>=current_reg<CR><Esc>
"
" Easy window navigation
noremap <C-h> <C-w>h
noremap <C-j> <C-w>j
noremap <C-k> <C-w>k
noremap <C-l> <C-w>l
"
" Split previously opened file ('#') in a split window
nnoremap <leader>w <C-w>v
nnoremap <leader>wj <C-w>S
" Sane splits
set splitright
set splitbelow
"
" Remap j and k to act as expected when used on long, wrapped, lines
nnoremap j gj
nnoremap k gk
"
" Clears the search register
nnoremap <silent> <leader>\ :nohlsearch<CR>
"
" Pull word under cursor into LHS of a substitute (for quick search and replace)
nnoremap <leader>r :%s#\<<C-r>=expand("<cword>")<CR>\>#
"
" Surround word with "
nnoremap <leader>" viw<esc>a"<esc>hbi"<esc>lel
vnoremap <leader>" <esc>a"<esc>h`<i"<esc>
nnoremap <leader>' viw<esc>a'<esc>hbi'<esc>lel
vnoremap <leader>' <esc>a'<esc>h`<i'<esc>
"
" Terminal remap
tnoremap <Esc> <C-w>N<CR>
set timeoutlen=300 " http://stackoverflow.com/questions/2158516/delay-before-o-opens-a-new-line
" set notimeout ttimeout timeoutlen=100
"
" Search in all git project exluding .git
map <leader>* :Ggrep --untracked <cword><CR><CR>
"
" }}}
"
" Functions {{{
"
" Zoom
function! s:zoom()
  if winnr('$') > 1
    tab split
  elseif len(filter(map(range(tabpagenr('$')), 'tabpagebuflist(v:val + 1)'),
        \ 'index(v:val, '.bufnr('').') >= 0')) > 1
    tabclose
  endif
endfunction
nnoremap <silent> <leader>z :call <sid>zoom()<cr>
" }}}
"
" File Type assosiacion {{{
"
" Python
" au BufNewFile,BufRead *.py
    " \ set tabstop=4 |
    " \ set softtabstop=4 |
    " \ set shiftwidth=4 |
    " \ set expandtab |
    " \ set autoindent |
    " \ set fileformat=unix

" Markdown
au BufNewFile,BufRead *.md
    \ set tabstop=4 |
    \ set softtabstop=4 |
    \ set shiftwidth=4 |
    \ set textwidth=79 |
    \ set expandtab |
    \ set autoindent |
    \ set fileformat=unix
" }}}
"
" Plugin setting {{{
"
" Netrw ----------------------------------------------------
let g:netrw_fastbrowse = 0
set nocp                    " 'compatible' is not set
filetype plugin on          " plugins are enabled
nnoremap <leader>e :Lexplore <CR>
let g:netrw_banner = 0			" disable annoying banner
let g:netrw_winsize = 20
let g:netrw_browse_split = 0	" open in prior window
let g:netrw_altv = 1			" open split to the right
let g:netrw_liststyle = 3		" tree view
let g:netrw_list_hide = netrw_gitignore#Hide()
let g:netrw_list_hide.= ',\(^\|\s\s\)zs\.\S\+'
"-----------------------------------------------------------
"
" vim-tmux-navigator ---------------------------------------
let g:tmux_navigator_no_mappings = 1
nnoremap <silent> <c-h> :TmuxNavigateLeft<cr>
nnoremap <silent> <c-j> :TmuxNavigateDown<cr>
nnoremap <silent> <c-k> :TmuxNavigateUp<cr>
nnoremap <silent> <c-l> :TmuxNavigateRight<cr>
nnoremap <silent> <c-\> :TmuxNavigatePrevious<cr>
"-----------------------------------------------------------
"
" switch ---------------------------------------------------
let g:switch_mapping = "-"
let g:switch_custom_definitions =
    \ [
    \   [0, 1],
    \   ["Disable", "Enable"],
    \   ["TRUE", "FALSE"],
    \   ["True", "False"]
    \ ]
"-----------------------------------------------------------
"
" base16 ---------------------------------------------------
" let base16colorspace=256
" colorscheme base16-flat
"-----------------------------------------------------------
"
" python-mode ----------------------------------------------
let g:pymode_python = 'python3'
let g:pymode_options_max_line_length = 120
let g:pymode_lint_options_pep8 = {'max_line_length': g:pymode_options_max_line_length}
let g:pymode_options_colorcolumn = 1
let g:python_rope = 0
let g:pymode_breakpoint = 0
" let g:python_run_bind = '<leader>n'
autocmd Filetype python nnoremap <buffer> <leader>n :w<CR>:vert rightb ter ++cols=85 python3 "%"<CR>
"-----------------------------------------------------------
"
" jedi-vim -------------------------------------------------
" autocmd FileType python setlocal completeopt-=preview
" " let g:jedi#auto_vim_configuration = 0
" set completeopt=menuone,longest
" let g:jedi#goto_command = "<leader>d"
" let g:jedi#goto_assignments_command = "<leader>g"
" let g:jedi#goto_definitions_command = ""
" let g:jedi#documentation_command = "K"
" let g:jedi#usages_command = "<leader>N"
" let g:jedi#completions_command = "<C-Space>"
" let g:jedi#rename_command = "<leader>r"
" let g:jedi#use_splits_not_buffers = "left"
" let g:jedi#popup_on_dot = 1
" let g:jedi#popup_select_first=0
" set completeopt=menuone,longest  " preview is not set since i don't like it
"-----------------------------------------------------------
"
"Calendar---------------------------------------------------
let g:calendar_first_day       = 'monday'
"-----------------------------------------------------------
"
" Clap ------------------------------------------------------
map <leader>f :Clap files<CR>
map <leader>b :Clap buffers<CR>
"-----------------------------------------------------------
"
"Easy Align-------------------------------------------------
vmap <Enter> <Plug>(EasyAlign)
"-----------------------------------------------------------
"
"TagBar http://mirnazim.org/writings/vim-plugins-i-use/-----
nnoremap <leader>l :TagbarToggle<CR>
"-----------------------------------------------------------
"
"NerdCommenter----------------------------------------------
let NERDSpaceDelims=1
let g:NERDDefaultAlign = 'left'
" vim 8 / neovim HEAD runtime: when ft==python, cms:=#\ %s
" "   -- when g:NERDSpaceDelims==1, then NERDComment results in double space
let g:NERDCustomDelimiters = {
            \ 'python': { 'left': '#', 'right': '' }
            \ }
"-----------------------------------------------------------
"
"a.vim------------------------------------------------------
" nnoremap <leader>A :AV<cr>
"-----------------------------------------------------------
"
" undotree -------------------------------------------------
nnoremap <leader>u :UndotreeToggle<cr>
"-----------------------------------------------------------
"
"Ale -------------------------------------------------------
" Enable completion where available.
let g:ale_set_balloons = 1
let g:ale_completion_enabled = 1
let g:ale_sign_column_always = 1
let g:ale_fix_on_save = 1
let g:ale_sign_column_always = 1
let g:airline#extensions#ale#enabled = 1
let g:ale_lint_on_text_changed = 'never'
let g:ale_fixers = {
			\   '*': ['remove_trailing_lines', 'trim_whitespace'],
			\   'python': [],
			\   'shell': ['shellcheck'],
			\   'rust': ['rustfmt'],
			\}

let g:ale_linters = {
			\	'python': ['pylint'],
			\}

" Python
let g:ale_python_pylint_options = '--rcfile ~/.pylintrc'

nmap <silent> <C-n> <Plug>(ale_previous_wrap)
nmap <silent> <C-p> <Plug>(ale_next_wrap)
nmap gd :ALEGoToDefinition<CR>
nmap gr :ALEFindReferences<CR>
nmap K :ALEHover<CR>
"-----------------------------------------------------------
"
" vim-airline ----------------------------------------------
let g:airline_theme='base16'
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '>'
let g:airline#extensions#ale#enabled = 1
let g:Powerline_symbols = "fancy"
set laststatus=2
"-----------------------------------------------------------
"
" grovebox -------------------------------------------------
let g:grovebox_itali = 1
let g:grovebox_contrast_dark = 'soft'
"-----------------------------------------------------------
"
" Ledger ---------------------------------------------------
let g:ledger_align_at = 40
"-----------------------------------------------------------
"
" Rust -----------------------------------------------------
let g:rustfmt_autosave = 1
let g:rustfmt_emit_files = 1
let g:rustfmt_fail_silently = 0
let g:rust_clip_command = 'xclip -selection clipboard'
"-----------------------------------------------------------
"
" Fuzzy Search ---------------------------------------------
" <leader>s for Rg search
noremap <leader>s :Rg<space>
let g:fzf_layout = { 'down': '~30%' }
command! -bang -nargs=* Rg
  \ call fzf#vim#grep(
  \   'rg --column --line-number --no-heading --color=always '.shellescape(<q-args>), 1,
  \   <bang>0 ? fzf#vim#with_preview('up:60%')
  \           : fzf#vim#with_preview('right:50%:hidden', '?'),
  \   <bang>0)
"-----------------------------------------------------------
"
" Vista ----------------------------------------------------
let g:vista_default_executive = 'ale'
let g:vista_fzf_preview = ['right:50%']
let g:vista_sidebar_width = 40
"-----------------------------------------------------------
"
" COC ------------------------------------------------------
" 'Smart' nevigation
" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
" inoremap <silent><expr> <TAB>
"       \ pumvisible() ? "\<C-n>" :
"       \ <SID>check_back_space() ? "\<TAB>" :
"       \ coc#refresh()

" function! s:check_back_space() abort
"   let col = col('.') - 1
"   return !col || getline('.')[col - 1]  =~# '\s'
" endfunction

" " Use <c-.> to trigger completion.
" inoremap <silent><expr> <c-.> coc#refresh()

" " Use <cr> to confirm completion, `<C-g>u` means break undo chain at current
" " position. Coc only does snippet and additional edit on confirm.
" if exists('*complete_info')
"   inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"
" else
"   imap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
" endif

" nmap <leader>rn <Plug>(coc-rename)
"-----------------------------------------------------------
"
" }}}
