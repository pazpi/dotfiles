#!/usr/bin/env bash

# Script for sending telegram message, the arguments of this script
# compose the message. Fill with your api and chat_id

API=""
CHAT_ID="28175961"

TEXT=$*
TEXT=$'Completato!\n'"$TEXT"

URL="https://api.telegram.org/bot$API/sendMessage"

OUTPUT=$(curl -s \
              -X POST \
              $URL \
              -d text="$TEXT" \
              -d chat_id="$CHAT_ID")
