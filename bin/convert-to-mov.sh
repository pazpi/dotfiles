#!/usr/bin/env bash

FILE=$1

if [ -e "$FILE" ]; then
    echo "Converting $FILE..."
    ffmpeg -y -i "$FILE" -c:v libxvid -c:v mpeg4 -force_key_frames "expr:gte(t,n_forced*1)" -b:v 250000k -c:a pcm_s16le "${FILE%%.*}.mov"
fi
