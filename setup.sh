#!/bin/bash

declare -A links=(
    # File
    [bashrc]=~/.bashrc
    [borg_exclude_dir]=~/.borg_exclude_dir
    [conkyrc]=~/.conkyrc
    [greenclip.cfg]=~/.config/greenclip.cfg
    [prompt_deadbeef_setup ]=~/.zprezto/modules/prompt/functions/prompt_deadbeef_setup
    [rtorrent.rc]=~/.rtorrent.rc
    [screenrc]=~/.screenrc
    [tmux.conf]=~/.tmux.conf
    [vimrc]=~/.vimrc
    [xinitrc]=~/.xinitrc
    [Xresources]=~/.Xresources
    [zpreztorc]=~/.zpreztorc
    [zshrc]=~/.zshrc
    # Directory
    [bin]=~/.bin
    [dunst]=~/.config/dunst
    [i3]=~/.config/i3
    [img]=~/.background
    [kitty]=~/.config/kitty
    [mpd]=~/.config/mpd
    [mpv]=~/.config/mpv
    [ncmpcpp]=~/.ncmpcpp
    [openbox]=~/.config/openbox
    [ranger]=~/.config/ranger
    [systemd]=~/.config/systemd
    [terminator]=~/.config/terminator
    [tint2]=~/.config/tint2
    [zsh]=~/.zsh
)

mode=$1
if [ "$mode" = "clean" ]; then
    echo "Removing links:"
    for i in "${!links[@]}"
    do
        echo "rm ${!links[$i]}"
        rm ${links[$i]}
    done
elif [ "$mode" = "new" ]; then
    echo "New installation:"
    for i in "${!links[@]}"
    do
        if [ ! -h  "${links[$i]}" ]
        then
            echo "$PWD/$i -> ${links[$i]}"
            ln -s $PWD/$i ${links[$i]}
        fi
    done
    curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    echo "Install minimun package:"
    echo "Please run as root:"
    echo "pacman -S `cat package-list.txt`"
else
    echo "creating new links:"
    for i in "${!links[@]}"
    do
        if [ ! -h  "${links[$i]}" ]
        then
            echo "$PWD/$i -> ${links[$i]}"
            ln -s $PWD/$i ${links[$i]}
        fi
    done
fi
